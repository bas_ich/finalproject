//https://www.youtube.com/watch?v=56E8b9prPTs//
import React, { Component } from 'react';
import './App.css';

import { BrowserRouter as Router, Route } from 'react-router-dom';

import Navbar from './pages/Navbar';
import Home from './pages/Home';
import User from './pages/User';
import Signup from './pages/Signup';
import Signin from './pages/Signin';
import Edit_profile from './pages/Edit/Edit_profile';
import Edit_password from './pages/Edit/Edit_password';
import Product from './pages/product/product';
import Test from './pages/test';
import Edit_product from './pages/product/Edit_product';
import Add_product from './pages/product/Add_product';
import Create_order from './pages/order/Create_order';
import Manage_order from './pages/order/Manage_orders';
import OrderTarder from './pages/order/product_information'
import ProductDetail from './pages/order/order_detail'
import ShowCart from './pages/order/show_cart '
import Status from './pages/order/Status'
import ChartBar from './pages/order/ChartBar'
import HTimeline from './pages/Timeline';
import SalesSum from './pages/SE_Middle/SalesSum';
import DetailOrder from './pages/SE_Middle/DetailOrder';
import Frequency from './pages/SE_Middle/frequency'

class App extends Component {
  render() {
    return (
      <Router exact path="/">
        <Navbar />

        <Route exact path='/' component={Home} />
        <Route exact path='/signin' component={Signin} />
        <Route exact path='/signup' component={Signup} />
        <Route exact path='/user' component={User} />
        <Route exact path='/user/edit_profile' component={Edit_profile} />
        <Route exact path='/user/edit_password' component={Edit_password} />
        <Route exact path='/product' component={Product} />
        <Route exact path='/edit_product' component={Edit_product} />
        <Route exact path='/add_product' component={Add_product} />
        <Route exact path="/create_order" component={Create_order} />
        <Route exact path='/test' component={Test} />
        <Route exact path="/manage_order" component={Manage_order} />
        <Route exact path="/manage_order/:orderID" component={Status} />
        <Route exact path="/product_information" component={OrderTarder} />
        <Route exact path="/product_information/:productID" component={ProductDetail} />
        <Route exact path="/show_cart" component={ShowCart}/>
        <Route exact path="/chart_bar" component={ChartBar}/>
        <Route exact path="/timeline" component={HTimeline}/>
        <Route exact path="/sales_sum" component={SalesSum}/>
        <Route exact path="/sales_sum/:orderID" component={DetailOrder}/>
        <Route exact path="/frequency" component={Frequency}/>
      </Router>
    )
  }
}

export default App;
