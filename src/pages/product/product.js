import React, { Component } from 'react';
import { get, post } from '../../service/service';
import { user_token } from '../../support/Constance';
import { Table, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product_data: [],
            pro_name: null,
            pro_amount: null,
            pro_cost: null,
            pro_details: null,
            pro_price: null,
            pro_status: null,
            pro_id: null
        };
    }

    componentWillMount() {
        this.get_product()
    }

    get_product = async () => {
        try {
            await get('product/show_product', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        product_data: result.result
                    })

                    setTimeout(() => {
                        console.log("get_product1", result.result)
                    }, 500)
                } else {
                    //window.location.href = "/";
                    alert(result.error_message)
                }
            });
        } catch (error) {
            alert("get_product2" + error);
        }
    }

    delete_array = (index) => {
        let product_data_array = this.state.product_data
        product_data_array.splice(index,1)
        this.setState({product_data:product_data_array})
    }

    delete = async (pro_id,index) => {
        let object = {
            pro_id: pro_id
        }
        try {
            await post(object, 'product/delete_product', user_token).then((res) => {
                if (res.success) {
                    alert("ลบสินค้าสำเร็จ")
                    // window.location.href = "/product";
                    this.delete_array(index)
                    // this.get_product()
                } else {
                    console.log(res.error_message)
                }
            })
        } catch (err) {
            console.log(object)
        }
    }


    render() {
        return (
            <div className="App">

                <h1> ข้อมูลสินค้า </h1>
                <Link to={{ pathname: "/add_product" }}>
                    <Button color="info">เพิ่มสินค้า</Button>
                </Link>
                <Table>
                    <tbody>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รหัสสินค้า</th>
                            <th>ชื่อสินค้า</th>
                            <th>สถานะ</th>
                            <th>ราคาขาย</th>
                            <th>จำนวนสินค้าในสต๊อก</th>
                            <th>รายละเอียด</th>
                            <th>แก้ไข</th>
                            <th>ลบ</th>
                        </tr>
                        {
                            this.state.product_data.map((element, index) => {
                                return (
                                    <tr>
                                        <td>{index + 1}</td>
                                        <td>SE_{element.pro_id}</td>
                                        <td>{element.pro_name}</td>
                                        <td>{element.pro_status}</td>
                                        <td>{element.pro_price}</td>
                                        <td>{element.pro_amount}</td>
                                        <td>{element.pro_details}</td>
                                        <td><Link to={{ pathname: "/edit_product", params: element }}><Button outline color="primary"  > แก้ไข </Button></Link></td>
                                        <td><Button outline color="danger" onClick={() => this.delete(element.pro_id,index)}> ลบ </Button></td>
                                    </tr>

                                )
                            })
                        }
                    </tbody>
                </Table>
            </div>

        )
    }
}

export default Product;