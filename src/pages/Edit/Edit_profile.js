import React, { Component } from 'react';
import '../../App.css';
import { get, post, ip } from '../../service/service';
import { user_token } from '../../support/Constance';

class Edit_profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            name: null,
            lastname: null,
            email: null,
            phone: null,
            address: null,
            user_image: null,
            // default_user_image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2S47oPrtWI_lK68iwye6EW3Q9GMRPoCQPw4vlObBssUl355pLMg"
        }

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentWillMount() {
        this.get_user();
    }

    get_user = async () => {
        try {
            await get('show/show_user', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        get_user: result.result
                    })
                    setTimeout(() => {
                        console.log("get_user_page_edit1", result.result)
                    }, 500)
                    this.updata()
                } else {
                    window.location.href = "/user";
                    //alert("user1"+result.error_message);
                }
            });
        } catch (error) {
            alert("get_user_page_edit2" + error);
        }
    }

    updata() {
        this.setState({
            username: this.state.get_user.username,
            password: this.state.get_user.password,
            email: this.state.get_user.email,
            name: this.state.get_user.name,
            lastname: this.state.get_user.lastname,
            phone: this.state.get_user.phone,
            address: this.state.get_user.address,
            user_image: this.state.get_user.user_image
        });
    }

    onSubmit(e) {
        const new_profile = {
            username: this.refs.username.value,
            password: this.refs.state.password.value,
            email: this.refs.state.email.value,
            name: this.refs.state.name.value,
            lastname: this.refs.state.lastname.value,
            phone: this.refs.state.phone.value,
            address: this.refs.state.address.value,
            user_image: this.refs.state.user_image.value
        }
        this.edit(new_profile);
        e.preventDefault();
    }

    edit = async () => {
        let object = {
            username: this.state.username,
            password: this.state.password,
            email: this.state.email,
            name: this.state.name,
            lastname: this.state.lastname,
            phone: this.state.phone,
            address: this.state.address,
            user_image: this.state.user_image
        };

        try {
            await post(object, "user/user_update_data", user_token)
                .then(res => {
                    console.log("edit1" + res);

                    if (res.success) {
                        alert("บันทึกข้อมูลเรียบร้อย");
                        window.location.href = "/user";
                    } else {
                        alert("edit_alert : " + res.error_message);
                    }
                });
        } catch (error) {
            alert(error);
        }
        console.log("edit2" + this.state);
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    uploadpicture = (e) => {

        let reader = new FileReader();
        let file = e.target.files[0];
        if (!file) {

        } else {
            reader.readAsDataURL(file)

            reader.onloadend = () => {
                console.log("img", reader.result)
                this.setState({
                    user_image: reader.result
                });
            }
        }

    }

    cencle = () => {
        window.location.href = "/user";
    }

    render() {
        return (
            <div className="App" >
                <div className="FontHeader"> เปลี่ยนข้อมูลผู้ใช้ </div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Side"></div>
                    <div className="Column Middle">
                    <div className="FontSignin">รูปผู้ใช้</div>
                    {this.state.user_image ?
                            <img className="PhotoProfile" src={ip + this.state.user_image} />
                            : <img className="PhotoProfile" src={ip + this.state.user_image} />
                        }
                    <input type="file"
                           onChange={(e) => this.uploadpicture(e)} />

                        <div className="FontSignin">ชื่อผู้ใช้</div>
                        <input
                            value={this.state.username}
                            type="text"
                            id="username"
                            placeholder="Enter your new username"
                            name="username"
                            onChange={this.handleInputChange} />
                        <div className="FontSignin">ชื่อ</div>
                        <input
                            value={this.state.name}
                            type="text"
                            id="name"
                            placeholder="Enter your new frist name"
                            name="name"
                            onChange={this.handleInputChange} />
                        <div className="FontSignin">นามสกุล</div>
                        <input
                            value={this.state.lastname}
                            type="text" id="lastname"
                            placeholder="Enter your new last name"
                            name="lastname"
                            onChange={this.handleInputChange} />
                        <div className="FontSignin">E-mail</div>
                        <input
                            value={this.state.email}
                            type="email"
                            id="email"
                            placeholder="Enter your new E-mail"
                            name="email"
                            onChange={this.handleInputChange} />
                        <div className="FontSignin">เบอร์โทรศัพท์</div>
                        <input
                            value={this.state.phone}
                            type="tel"
                            id="phone"
                            placeholder="Enter your new phone"
                            name="phone"
                            onChange={this.handleInputChange} />
                        <div className="FontSignin">ที่อยู่</div>
                        <input
                            value={this.state.address}
                            type="text"
                            id="address"
                            placeholder="Enter your new address"
                            name="address"
                            onChange={this.handleInputChange} />
                        
                        
                        <button
                            onClick={
                                () => this.edit(
                                    this.state.username,
                                    this.state.email,
                                    this.state.name,
                                    this.state.lastname,
                                    this.state.phone,
                                    this.state.address,
                                    this.state.user_image

                                )
                            } className="BTNSignup">
                            บันทึก
                        </button>
                        <button className="BTNSignup" onClick={() => this.cencle()}>ยกเลิก</button>
                    </div>
                    <div className="Column Side"></div>
                    <div className="Column Side"></div>
                </div>
            </div>
        );
    }
}

export default Edit_profile;
