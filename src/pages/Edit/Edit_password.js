import React, { Component } from 'react';
import { post } from '../../service/service';
import { user_token } from '../../support/Constance';
import '../../App.css'



class Edit_password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: null,
            newpassword: null,
            c_newpassword: null
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleSubmit = () => {
        // perform all neccassary validations
        if (this.state.newpassword !== this.state.c_newpassword) {
            alert("รหัสผ่านไม่ตรงกัน");
        } else {
            this.edit();
            // this.setState ({ _password : this.state.newpassword })
            // this.edit();
            // console.log("edit_submit : " + this.state.password + "   edit : " + this.state.newpassword + "   edit : " + this.state._password);
        }

    }

    edit = async () => {
        let object = {
            password: this.state.password,
            newpassword: this.state.newpassword
        };
        console.log("obj_pass : " + object.password + "   obj_newpass : " + object.newpassword + "   obj__pass : " + object._password)
        try {
            await post(object, "user/user_update_password", user_token)
                .then(res => {


                    if (res.success) {
                        alert("เปลี่ยนรหัสผ่านเรียบร้อย");
                        window.location.href = "/user";
                    } else {
                        alert("error 2 : " + res.error_message);
                    }
                });
        } catch (error) {
            alert("error3" + error);
        }
    }

    cencle = () => {
        window.location.href = "/user";
    }

    render() {
        return (
            <div className="App" >
                <div className="FontHeader"> เปลี่ยนรหัสผ่าน </div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Side"></div>
                    <div className="Column Middle">
                        <div className="FontSignin">รหัสผ่านเดิม</div>
                        <input
                            type="password"
                            id="password"
                            placeholder="Enter your password"
                            name="password"
                            onChange={this.handleChange}
                        />
                        <div className="FontSignin">รหัสผ่านใหม่</div>
                        <input
                            type="password"
                            id="newpassword"

                            placeholder="Enter your new password"
                            name="newpassword"
                            onChange={this.handleChange}
                        />
                        <div className="FontSignin">ยืนยันรหัสผ่าน</div>
                        <input
                            type="password"
                            id="c_newpassword"

                            placeholder="Enter your new password agian"
                            name="c_newpassword"
                            onChange={this.handleChange}
                        />
                        <button className="BTNSignup"
                            onClick={
                                () => this.handleSubmit(
                                    this.state.password,
                                    this.state.newpassword,
                                    this.state.c_newpassword
                                )
                            }>
                            ยืนยัน
                    </button>
                        <button className="BTNSignup" onClick={() => this.cencle()}>ยกเลิก</button>
                    </div>
                    <div className="Column Side"></div>
                    <div className="Column Side"></div>
                </div>
            </div>
        );
    }
}

export default Edit_password;
