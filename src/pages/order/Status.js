import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { get, post } from '../../service/service';
import { user_token, addComma } from '../../support/Constance';
import moment from 'moment'
import queryString from 'query-string';
import HTimeline from '../Timeline';
import Pdf from './PDF'

class Status extends Component {
    constructor(props) {
        super(props)
        this.state = {
            order: [],
            detail: [],
            plant: [],
            data: [],
            photo_profile: "https://i.stack.imgur.com/l60Hf.png",
            tag0: "https://image.flaticon.com/icons/svg/1161/1161832.svg",
            tag1: "https://image.flaticon.com/icons/svg/1161/1161833.svg",
            tag_default: "https://image.flaticon.com/icons/svg/1161/1161830.svg"
        }
    }


    render_status = (order_status) => {
        let render_tag

        switch (order_status) {
            case 0:
                render_tag = <div>
                    <img className="tag" src={this.state.tag0} />
                    <div className="FontWarning" > กำลังดำเนินการ </div>
                </div>
                break;
            case 1:
                render_tag = <div>
                    <img className="tag" src={this.state.tag1} />
                    <div className="FontSuccess"> สำเร็จแล้ว </div>
                </div>
                break;
            default:
                render_tag = <div>
                    <img className="tag" src={this.state.tag_default} />
                    <div className="FontDanger"> เกิดข้อผิดพลาด </div>
                </div>
                break;
        }
        return render_tag
    }


    componentWillMount() {
        this.get_order()
    }

    get_order = async () => {
        let url = this.props.location.search;
        let params = queryString.parse(url);
        try {
            await post(params, 'trader/get_order_info', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        order: result.result,
                        detail: result.result.detail,
                        plant: result.result.plant,
                    })
                    setTimeout(() => {
                        console.log("get_product1", result.result)
                    }, 500)
                } else {
                    window.location.href = "/manage_order";
                    alert(result.error_message)
                }
            });
        } catch (error) {
            alert("get_cart_trader" + error);
        }
    }

    // sum_data = (data) => {
    //     let sum = 0;

    //     for (var i = 0; i < data.length; i++) {
    //         let data_sum = data[i];
    //         sum += data_sum;
    //         sum.toLocaleString()
    //     }
    //     return sum;
    //     // return sum.toLocaleString();
    // }
    sum_price = (data) => {
        let sum = 0;
        data.map((element) => {
            sum += (element.price * element.amount)
        })
        return sum;

    }


    render() {

        return (
            <div className="App">
                <div className="FontHeader">รายละเอียดการสั่งซื้อ</div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Middle">
                        <div>เลขที่ใบสั่งซื้อ : {this.state.order.order_id}</div>
                        <div>วันที่ : {moment(this.state.order.order_date).utc().add('years', 543).format("DD/MM/YYYY, HH:mm")}</div>
                        <div>สถานะ : {this.render_status(this.state.order.order_status)}
                        </div>

                        <Pdf data={this.state.order} />

                        <HTimeline />
                        <div className="card">
                            <div className="container">
                                <h5>สถานะการสั่งซื้อ : รอยืนยันคำสั่งซื้อ</h5>
                                <p>ส่งใบสั่งซื้อแล้วรอ SE กลาง ส่งใบแจ้งหนี้กลับมา</p>
                            </div>
                        </div>
                        <div>รายการวัตถุดิบ</div>

                        <table>
                            <tr>
                                <th>ลำดับ</th>
                                <th>รหัสวิตถุดิบ</th>
                                <th>ชื่อวัตถุดิบ</th>
                                <th>ราคา/หน่วย</th>
                                <th>จำนวน</th>
                                <th>จำนวนเงิน</th>
                            </tr>
                            {
                                this.state.detail.map((element_plant, index) => {
                                    return (
                                        <tr>
                                            <td><div style={{ textAlign: "center" }}>{index + 1}</div></td>
                                            <td>{element_plant.plant_id}</td>
                                            <td>{element_plant.plant_name}</td>
                                            <td><div style={{ textAlign: "center" }}>{element_plant.price}</div></td>
                                            <td><div style={{ textAlign: "center" }}>{addComma(element_plant.amount)} กิโลกรัม</div></td>
                                            <td><div style={{ textAlign: "right" }}>{addComma(element_plant.price * element_plant.amount)}</div></td>
                                        </tr>

                                    )
                                })
                            }
                        </table>
                        <div style={{ textAlign: "right" }} className="FontBody">
                            <h4>รวมทั้งสิ้น : {addComma(this.sum_price(this.state.detail))} บาท</h4>
                        </div>
                    </div>
                    <div className="Column Side"></div>
                </div>
            </div>
        )
    }
}

export default Status;