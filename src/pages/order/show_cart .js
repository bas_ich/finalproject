import queryString from 'query-string';
import React, { Component } from 'react';
import { get, post } from '../../service/service';
import { user_token, addComma } from '../../support/Constance';
import { Link } from 'react-router-dom';


class UserComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cart_product: [],
            render_cart: null,
            total_price: []
        }
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleInputChange = (event, index) => {
        const value = event.target.value;

        let cart_product = this.state.cart_product //ตั้งตัวแปรแแทน state

        cart_product[index].amount = value
        this.setState({
            cart_product: cart_product
        })
    }

    componentWillMount() {
        this.get_product()
    }

    get_product = async () => {
        try {
            await get('trader/get_cart_trader', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        cart_product: result.result,
                        render_cart: 1
                    })
                    setTimeout(() => {
                        console.log("cart_product1", result.result)
                    }, 500)
                } else {
                }
            });
        } catch (error) {
            alert("get_cart_trader" + error);
        }
    }


    edit_amount = async () => {
        console.log(this.state.cart_product)

        let object = {
            data: this.state.cart_product
        }
        console.log(object)
        if (window.confirm("ยืนยันการแก้ไขจำนวน")) {
            try {
                await post(object, 'trader/update_cart_trader', user_token).then((result) => {
                    if (result.success) {
                        alert(result.message)
                    }
                    else {
                        window.location.href = "/product_information";
                        alert(result.error_message)
                    }
                })

            }
            catch (error) {
                alert("update_cart_trader" + error);
            }
        }

    }

    delete_product = async (plant_id) => {
        let object = {
            plant_id: plant_id
        }
        try {
            await post(object, 'trader/delete_cart_product_tarder', user_token).then((result) => {
                if (result.success) {
                    alert(result.message)
                    window.location.href = "/show_cart";
                }
                else {
                    window.location.href = "/product_information";
                    alert(result.error_message)
                }
            })
        }
        catch (error) {
            alert("delete_cart_product_tarde" + error);
        }
    }

    Comfirm = async () => {
        let object = {
            detail: this.state.cart_product,
            order_status: "0"
        }
        try {
            await post(object, 'trader/add_order', user_token).then((result) => {
                if (result.success) {
                    alert("ระบบดำเนินการส่งใบสั่งซื้อเรียบร้อย")
                    window.location.href = "/manage_order";
                }
                else {
                    alert(result.error_message)
                }
            })

        }
        catch (error) {
            alert("get_cart_trader" + error);
        }

    }
    sum_price = (data) => {
        let sum = 0;
        data.map((element) => {
            sum += (element.price * element.amount)
        })

        return sum;
        // return sum.toLocaleString();
    }

    render() {
        const have_cart = (
            <div className="App">
                <div className="FontHeader">ตระกร้าสินค้า</div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Middle">
                        <table>
                            <tr>
                                <th rowspan="1">ลำดับ</th>
                                <th rowspan="1">รหัสสินค้า</th>
                                <th rowspan="1">ชื่อสินค้า</th>
                                <th rowspan="1">จำนวน</th>
                                <th>ราคา</th>
                                <th>ลบ</th>
                            </tr>

                            {this.state.cart_product.map((element, index) => {
                                return (
                                    <tr>
                                        <td><div style={{ textAlign: "center" }}>{index + 1}</div></td>
                                        <td>{element.plant_id}</td>
                                        <td>{element.plant_name}</td>
                                        <td><input type="number" name="amount"
                                            value={element.amount}
                                            onChange={(event) => { this.handleInputChange(event, index) }} /></td>
                                        <td><div style={{ textAlign: "right" }}>{addComma(element.price * element.amount)}</div></td>
                                        <td><button className="BTNCencleProduct"
                                            onClick={() => { this.delete_product(element.plant_id) }}>ลบ</button></td>

                                    </tr>
                                )
                            })}
                            <tr>
                                <td colSpan="4"><div style={{ textAlign: "right" }}>รวมทั้งสิ้น : </div></td>
                                <td colSpan="1"><div style={{ textAlign: "right" }}>
                                    <h4>{addComma(this.sum_price(this.state.cart_product))} </h4>
                                </div></td>
                                <td><h4>บาท</h4></td>
                            </tr>
                        </table>
                        <button className="BTNComfirm" onClick={() => { this.Comfirm() }}>ยืนยันสั่งซื้อสินค้า</button>
                        <button className="BTNEdit" onClick={() => { this.edit_amount() }}>แก้ไข</button>

                    </div>
                    <div className="Column Side"></div>
                </div>
            </div >
        )

        const not_cart = (
            <div className="App">
                <div className="FontHeader">ตระกร้าสินค้า</div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Middle">
                        <img className="HaveNotCart" src={"https://image.flaticon.com/icons/svg/138/138275.svg"} />
                        <div className="FontBody">ไม่มีสินค้าในตะกร้า</div>
                    </div>
                    <div className="Column Side"></div>
                </div>
            </div >
        )

        return (
            <div>
                {this.state.render_cart ? have_cart : not_cart}
            </div >
        )

    }
}
export default UserComponent