import queryString from 'query-string';
import React, { Component } from 'react';
import { ip, get, post } from '../../service/service';
import { user_token,addComma } from '../../support/Constance';
import { Link, NavLink } from 'react-router-dom';
import ChartBar from './ChartBar';

class UserComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            product_data: [],
            plant: [],
            plant_id: [],
            amount: 1,
            data: [],
            data_cart: [],
            total_plant: [],
            total_price:[]
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    componentWillMount() {
        this.get_product()
    }

    get_product = async () => {
        let url = this.props.location.search;
        let params = queryString.parse(url);
        console.log(params);
        try {
            await post(params, 'trader/get_product_information', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        product_data: result.result,
                        plant: result.result.plant
                    })

                    setTimeout(() => {
                        console.log("get_product1", result.result)
                    }, 500)
                } else {
                    window.location.href = "/product_information";
                    alert(result.error_message)
                }
            });
        } catch (error) {
            alert("get_product2" + error);
        }
    }

    add_cart = async () => {

        this.state.plant.map((element, index) => {
            this.state.data.push({
                plant_id: element.plant_id,
                total_plant: this.state.total_plant = element.volume * this.state.amount,
                // total_price: this.state.total_price = element.price * this.state.total_plant
            })
        })

        let data = {
            data_plant: this.state.data
        }
        try {
            await post(data, 'trader/add_cart_trader', user_token).then((result) => {
                if (result.success) {
                    // data_cart.push()
                    alert("เพิ่มลงในตะกร้าแล้ว " + this.state.amount + " ชิ้น");

                    setTimeout(() => {
                        console.log("get_product11", result)
                    }, 500)
                } else {
                    alert(result.error_message)
                }
            });
        } catch (error) {
            alert("get_product22" + error);
        }
    }

    sum_data = (data) => {
        let sum = 0;

        for (var i = 0; i < data.length; i++) {
            let data_sum = data[i];
            sum += data_sum;
            sum.toLocaleString()
        }
        return sum;
        // return sum.toLocaleString();
    }


    render() {

        return (
            <div className="App">
                <div className="FontHeader">รายละเอียด</div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Half">
                        <img src={ip + 'trader/image/' + this.state.product_data.image} className="PhotoProduct" />
                    </div>
                    <div className="Column Half">
                        <div className="FontBody">{this.state.product_data.product_name}</div>
                        <div className="FontDetial">{this.state.product_data.product_status}</div>
                        <input type="number"
                            name="quantity" min="1"
                            id="amount" placeholder="1"
                            onChange={this.handleChange} />
                        <table>
                            <tr>
                                <th>รหัส</th>
                                <th>ชื่อพืช</th>
                                <th>ราคา/กิโลกรัม</th>
                                <th>จำนวนที่ใช้</th>
                                <th>ราคา</th>
                            </tr>

                            {this.state.plant.map((element, index) => {
                                return (
                                    <tr>
                                        <td>{element.plant_id}</td>
                                        <td>{element.plant_name}</td>
                                        <td>{addComma(element.price)}</td>
                                        <td>{addComma(this.state.total_plant = element.volume * this.state.amount)}</td>
                                        <td>{addComma(this.state.total_price = element.price * this.state.total_plant)}</td>
                                        
                                    </tr>
                                )
                            })}

                        </table>
                        <button className="BTNAddProduct" onClick={() => { this.add_cart() }}>เพิ่มลงตะกร้า</button>
                    </div>
                    <div className="Column Side"></div>
                </div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Middle">
                        <table>
                            <tr>
                                <th>ชื่อพืช</th>
                                <th>รหัส</th>
                                <th>จำนวนที่ใช้</th>
                                <th>ที่มีอยู่</th>
                                <th>จำนวนที่ขาด</th>
                            </tr>

                            {this.state.plant.map((element, index) => {
                                return (
                                    <tr>
                                        <td>{element.plant_name}</td>
                                        <td>{element.plant_id}</td>
                                        <td>{addComma(this.state.total_plant = element.volume * this.state.amount)}</td>
                                        <td>{addComma(this.sum_data(element.data))}</td>
                                        <td>{this.sum_data(element.data) - this.state.total_plant < 0 ? addComma(this.state.total_plant - this.sum_data(element.data)) : "วัตถุดิบมีเพียงพอ"}</td>
                                    </tr>
                                )
                            })}

                        </table>
                        <div className="FontBody">รายละเอียดการส่งมอบ</div>

                        {this.state.plant.map((element, index) => {
                        //    let data = element.data
                        //    let data_send = []
                        // //    console.log(data)
                        //    data.map((element_data)=>{
                        //        data_send.push(addComma(element_data))
                        //    })
                        //    console.log('gg',data_send)
                           
                            return (
                                <div>
                                    <ChartBar
                                        data={element.data}
                                        name={element.plant_name} />
                                </div>
                            )
                        })}

                    </div>
                    <div className="Column Side"></div>
                </div>
            </div>
        )
    }
}
export default UserComponent