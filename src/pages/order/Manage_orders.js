import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { get, ip } from '../../service/service';
import { user_token } from '../../support/Constance';
import moment from 'moment'


class Manage_order extends Component {
    constructor(props) {
        super(props)
        this.state = {
            order: [],
            get_user: null,
            get_product: null,
            search_order: [],
            render_history: null,
            photo_profile: "https://i.stack.imgur.com/l60Hf.png"
        }
    }

    filterID = (event) => {
        var updatedList = this.state.order;
        updatedList = updatedList.filter(function (item) {
            return item.order_id.toLowerCase().search(
                event.target.value.toLowerCase()) !== -1;
        });
        this.setState({
            search_order: updatedList,
        });
    }

    filterDate = (event) => {
        var updatedList = this.state.order;
        updatedList = updatedList.filter(function (item) {
            return item.order_date.toLowerCase().search(
                event.target.value.toLowerCase()) !== -1;
        });
        this.setState({
            search_order: updatedList,
        });
    }

    render_status = (order_status) => {
        let render_tag

        switch (order_status) {
            case 0:
                render_tag = <div className="FontWarning" > กำลังดำเนินการ </div>
                break;
            case 1:
                render_tag = <div className="FontSuccess"> สำเร็จแล้ว </div>
                break;
            default:
                render_tag = <div className="FontDanger"> เกิดข้อผิดพลาด </div>
                break;
        }
        return render_tag
    }

    componentWillMount() {
        this.get_order()
        this.get_user()
    }

    get_user = async () => {
        try {
            await get('show/show_user', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        get_user: result.result
                    })
                    setTimeout(() => {
                        console.log("get_user", result.result)
                    }, 500)
                } else {
                    window.location.href = "/";
                    //alert("user1"+result.error_message);
                }
            });
        } catch (error) {
            alert("get_user2" + error);
        }
    }

    get_order = async () => {
        try {
            await get('trader/get_order', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        order: result.result,
                        search_order: result.result,
                        render_history: 1
                    })

                    setTimeout(() => {
                        console.log("get_product1", result.result)
                    }, 500)
                } else {
                    window.location.href = "/product_information";
                    alert(result.error_message)
                }
            });
        } catch (error) {
            alert("get_cart_trader" + error);
        }
    }

    render_type = (user_type) => {
        let render_user
        switch (user_type) {
            case "2":
                render_user = <div > ผู้ประกอบการ </div>
                break;
            case "4":
                render_user = <div > SE กลาง </div>
                break;
            case "3":
                render_user = <div > SE ย่อย </div>
                break;
            case "1":
                render_user = <div > นักวิจัย </div>
                break;
            case "5":
                render_user = <div > Admin </div>
                break;
            default:
                render_user = <div className="FontDanger"> เกิดข้อผิดพลาด </div>
                break;
        }
        return render_user
    }

    render() {
        const have_history = (
            <div className="App">
                <div className="FontHeader">ประวัติการซื้อ</div>
                <div className="Row">
                    <div className="Column Side">
                        {this.state.get_user ? <img className="PhotoProfile" src={ip + this.state.get_user.user_image} />
                            : <img className="PhotoProfile" src={this.state.photo_profile} />}

                        <div className="FontBody">{this.state.get_user ? this.state.get_user.username : null}</div>
                        <div className="FontSide">{this.state.get_user ? this.render_type(this.state.get_user.user_type) : null}</div>
                        <ul>
                            <li><button className="BTNProcess" >กำลังดำเนินการ</button></li>
                            <li><button className="BTNSuccess" >สำเร็จเเล้ว</button></li>
                        </ul>
                    </div>
                    <div className="Column Middle">
                        <table>
                            <tbody>
                                
                                <td><input type="date" placeholder="กรอกรหัสวันที่ใบสั่งซื้อ" onChange={this.filterDate} /></td>
                                <td><input type="search" placeholder="กรอกรหัสใบสั่งซื้อ" onChange={this.filterID} /></td>
                                <td></td>
                                <td></td>
                            </tbody>
                            <tr>
                                <th>วันที่ใบสั่งซื้อ</th>
                                <th>เลขที่ใบสั่งซื้อ</th>
                                <th>สถานะสินค้า</th>
                                <th>รายละเอียด</th>
                            </tr>
                            {
                                this.state.search_order ?
                                    this.state.search_order.map((element, index) => {
                                        return (
                                            <tr>
                                                <td>{moment(element.order_date).utc().add('years', 543).format("DD/MM/YYYY, HH:mm")}</td>
                                                <td>{element.order_id}</td>
                                                <td>{this.render_status(element.order_status)}</td>
                                                <td><NavLink to={"/manage_order/order?order_id=" + element.order_id} className="Link">รายละเอียด</NavLink></td>
                                            </tr>
                                        )
                                    })
                                    :
                                    this.state.order.map((element, index) => {
                                        return (
                                            <tr>
                                                <td>{index + 1}</td>
                                                <td>{moment(element.order_date).utc().format("DD/MM/YYYY, HH:mm")}</td>
                                                <td>{element.order_id}</td>
                                                <td>{this.render_status(element.order_status)}</td>
                                                <td><NavLink to={"/manage_order/order?order_id=" + element.order_id} className="Link">รายละเอียด</NavLink></td>
                                            </tr>
                                        )
                                    })
                            }
                        </table>
                    </div>
                </div>
            </div>
        )

        const no_history = (
            <div className="App">
                <div className="FontHeader">ประวัติการซื้อ</div>
                <div className="Row">
                    <div className="Column Side">
                        {this.state.get_user ? <img className="PhotoProfile" src={ip + this.state.get_user.user_image} />
                            : <img className="PhotoProfile" src={this.state.photo_profile} />}

                        <div className="FontBody">{this.state.get_user ? this.state.get_user.username : "Name"}</div>
                        <div className="FontSide">{this.state.get_user ? this.render_type(this.state.get_user.user_type) : "Status"}</div>
                        <ul>
                            <li><button className="BTNProcess" >กำลังดำเนินการ</button></li>
                            <li><button className="BTNSuccess" >สำเร็จเเล้ว</button></li>
                        </ul>
                    </div>
                    <div className="Column Middle">
                        <div className="FontBody">ไม่มีประวัติการสั่งซื้อสินค้า</div>
                    </div>
                </div>
            </div>
        )
        return (
            <div>
                {this.state.render_history ? have_history : no_history}
            </div>
        )
    }
}

export default Manage_order;