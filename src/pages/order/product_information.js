import React, { Component } from 'react';
import { get, ip } from '../../service/service';
import { user_token } from '../../support/Constance';
import { Link } from 'react-router-dom';


class order_tarder extends Component {
    constructor(props) {
        super(props)
        this.state = {
            product_data: [],
            search_order: []
        }
    }

    componentWillMount() {
        this.get_product()
    }

    get_product = async () => {
        try {
            await get('trader/get_product', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        product_data: result.result,
                        search_order: result.result,
                    })

                    setTimeout(() => {
                        console.log("get_product1", result.result)
                    }, 500)
                } else {
                    window.location.href = "/";
                    alert(result.error_message)
                }
            });
        } catch (error) {
            alert("get_product2" + error);
        }
    }

    filterSearch = (event) => {
        var updatedList = this.state.product_data;
        updatedList = updatedList.filter(function (item) {
            return item.product_name.toLowerCase().search(
                event.target.value.toLowerCase()) !== -1;
        });
        this.setState({
            search_order: updatedList,
        });
    }

    render() {
        return (
            <div className="App">
                <div className="FontHeader">สินค้า</div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Middle">
                        <input type="search" placeholder="Search" onChange={this.filterSearch} />
                    </div>
                    <div className="Column Side"></div>
                </div>
                <div className="Row">
                    <div className="Column">

                        {
                            this.state.search_order ?
                                this.state.search_order.map((element, index) => {
                                    return (
                                        <div className="Card">
                                            <img alt="Product" className="PhotoProduct" src={ip + 'trader/image/' + this.state.product_data.image} />
                                            <div className="FontCard">{element.product_name}</div>
                                            {/* <div className="CardDetial">{element.product_status}</div> */}
                                            <Link to={"/product_information/product?product_id=" + element.product_id}><button className="BTNDetial">รายละเอียดเพิ่มเติม</button></Link>
                                        </div>
                                    )
                                })

                                :

                                this.state.product_data.map((element, index) => {
                                    return (
                                        <div className="Card">
                                            <img alt="Product" className="PhotoProduct" src={ip + 'trader/image/' + this.state.product_data.image} />
                                            <div className="FontCard">{element.product_name}</div>
                                            {/* <div className="CardDetial">{element.product_status}</div> */}
                                            <Link to={"/product_information/product?product_id=" + element.product_id}><button className="BTNDetial">รายละเอียดเพิ่มเติม</button></Link>
                                        </div>
                                    )
                                })
                        }
                    </div>
                </div>
            </div>
        )
    }

}
export default order_tarder;