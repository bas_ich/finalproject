import React, { Component } from "react";
import ReactApexChart from "react-apexcharts";
// import NumberFormat from 'react-number-format';
// import queryString from 'query-string';
// import { ip, get, post } from '../../service/service';
// import { user_token, addComma } from '../../support/Constance';
// import { Link, NavLink } from 'react-router-dom';

class ChartBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data_series:[],
            options: {
                plotOptions: {
                    bar: {
                        dataLabels: {
                            position: 'top', // top, center, bottom
                        },
                    }
                },
                dataLabels: {
                    enabled: true,
                    formatter: function (val) {
                        return val + "Kg.";
                    },
                    offsetY: -20,
                    style: {
                        fontSize: '12px',
                        colors: ["#304758"]
                    }
                },
                xaxis: {
                    categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    position: 'buttom',
                    labels: {
                        offsetY: 0,
                    },
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false
                    },
                    crosshairs: {
                        fill: {
                            type: 'gradient',
                            gradient: {
                                colorFrom: '#D8E3F0',
                                colorTo: '#BED1E6',
                                stops: [0, 100],
                                opacityFrom: 0.4,
                                opacityTo: 0.5,
                            }
                        }
                    },
                    tooltip: {
                        enabled: true,
                        offsetY: -45,
                    }
                },
                fill: {
                    type: 'pattern',
                    pattern: {
                        style: 'horizontalLines',
                        width: 6,
                        height: 6,
                        strokeWidth: 2
                    },
                },
                yaxis: {
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false,
                    },
                    labels: {
                        show: false,
                        formatter: function (val) {
                            return val + "Kg.";
                        }
                    }
                },
                title: {
                    text: 'กราฟแสดงจำนวนวัตถุดิบที่พร้อมส่งมอบ : ' + this.props.name,
                    floating: true,
                    position: 'buttom',
                    offsetY: 0,
                    align: 'left',
                    style: {
                        color: '#444'
                    }
                },
                plant: {
                    text: 'ชื่อพืช',
                    floating: true,
                    position: 'left',
                    offsetY: 0,
                    align: 'left',
                    style: {
                        color: '#444'
                    }
                },
            },

            series: [{
                name: this.props.name,
                data: this.props.data
            }],
            
        }
        
    }

    render() {
        return (
            <div className="App">
                <div id="chart">
                    <ReactApexChart
                        options={this.state.options}
                        series={this.state.series}
                        type="bar"
                        height="500"
                        width="100%" />
                </div>
            </div>
        );
    }
}

export default ChartBar;