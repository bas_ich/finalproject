import React, { Component } from 'react';
import '../App.css';
import { post } from '../service/service';


class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null,
            email: null,
            name: null,
            lastname: null,
            phone: null,
            address: null,
            user_type: 1
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    show_password() {
        var show_password = document.getElementById("password");
        if (show_password.type === "password") {
            show_password.type = "text";
        } else {
            show_password.type = "password";
        }
    }

    add_user = async () => {
        let object = {
            username: this.state.username,
            password: this.state.password,
            email: this.state.email,
            name: this.state.name,
            lastname: this.state.lastname,
            phone: this.state.phone,
            address: this.state.address,
            user_type: this.state.user_type
        };

        try {
            await post(object, "user/user_register", null).then(res => {
                console.log("Signup" + res);

                if (res.success) {
                    alert("สร้างบัญชีผู้ใช้เรียบร้อย โปรดทำการเข้าสู่ระบบ");
                    window.location.href = "/signin";
                } else {
                    alert(res.error_message);
                }
            });
        } catch (error) {
            alert(error);
        }
        console.log("Signup" + this.state);
    }

    ChangePage = () => {
        window.location.href = "/signin"
    }

    render() {
        return (
            <div className="App" >
                <div className="FontHeader"> ลงทะเบียน </div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Side"></div>
                    <div className="Column Middle">
                        <div className="FontSignin">ชื่อ</div>
                        <input type="text" id="name" placeholder="กรอกชื่อจริง" name="name" onChange={this.handleChange} />
                        <div className="FontSignin">นามสกุล</div>
                        <input type="text" id="lastname" placeholder="กรอกนามสกุล" name="lastname" onChange={this.handleChange} />
                        <div className="FontSignin">E-mail</div>
                        <input type="email" id="email" placeholder="กรอก E-mail" name="email" onChange={this.handleChange} />
                        <div className="FontSignin">เบอร์โทรศัพท์</div>
                        <input type="tel" id="phone" placeholder="กรอกเบอร์โทรศํพท์" name="phone" onChange={this.handleChange} />
                        <div className="FontSignin">ที่อยู่</div>
                        <input type="text" id="address" placeholder="กรอกที่อยู่ปัจจุบัน" name="address" onChange={this.handleChange} />
                        <div className="FontSignin">ชื่อบัญชีผู้ใช้</div>
                        <input type="text" id="username" placeholder="กรอกบัญชีผู้ใช้" name="username" onChange={this.handleChange} />
                        <div className="FontSignin">รหัสผ่าน</div>
                        <input type="password" id="password" placeholder="กรอกรหัสผ่าน" name="password" onChange={this.handleChange} />
                        <input type="checkbox" id="show_password" name="show_password" onClick={this.show_password} />แสดงรหัสผ่าน
                        <div>
                            <button className="BTNSignup"
                                onClick={() => this.add_user(
                                    this.state.username,
                                    this.state.password,
                                    this.state.email,
                                    this.state.name,
                                    this.state.last_name,
                                    this.state.phone,
                                    this.state.address,
                                    this.state.user_type
                                )}>
                                ลงทะเบียน
                        </button>
                            <button className="BTNSignup" onClick={() => this.ChangePage()}>มีบัญชีผู้ใช้เเล้ว</button>
                        </div>
                    </div>
                    <div className="Column Side"></div>
                    <div className="Column Side"></div>
                </div>
            </div>
        );
    }
}

export default Signup;
