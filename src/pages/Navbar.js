import React, { Component } from 'react';
import '../App.css';
import { withRouter, NavLink } from 'react-router-dom';
import { user_token } from '../support/Constance';
import { get, ip } from '../service/service';

class Nav_bar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            get_user: null,
        };
    }

    get_user = async () => {
        try {
            await get('show/show_user', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        get_user: result.result
                    })
                    setTimeout(() => {
                        console.log("get_user", result.result)
                    }, 500)
                } else {
                    //alert("user1"+result.error_message);
                }
            });
        } catch (error) {
            alert("get_user2" + error);
        }
    }

    componentWillMount() {
        this.get_user()
    }

    render_type = (user_type) => {
        let render_user
        switch (user_type) {
            case "1":
                render_user =<div>นักวิจัย</div>
                break;

            case "2": //ผู้ประกอบการ
                render_user =
                    <div className="App Sticky">
                        <div className="TopBar">
                            <NavLink exact to="/" className="LogoBar">SE Home Shop</NavLink>

                            <NavLink to="/signin" activeClassName="active" className="TopBarRight" onClick={this.logOut.bind(this)} >ออกจากระบบ</NavLink>
                            <NavLink to="/user" activeClassName="active" className="TopBarRight">
                                {this.state.get_user ? this.state.get_user.username : null}
                            </NavLink>
                            {this.state.get_user ? <img alt="PhotoUser" className="PhotoProfileBar" src={ip + this.state.get_user.user_image} />
                                : <img alt="PhotoUser" className="PhotoProfileBar" src={this.state.photo_profile} />}


                            <NavLink to="/manage_order" activeClassName="active" className="TopBarRight">ประวัติการซื้อ</NavLink>
                            <NavLink to="/show_cart" activeClassName="active" className="TopBarRight">ตะกร้าสินค้า</NavLink>
                            <NavLink to="/product_information" activeClassName="active" className="TopBarRight">สินค้า</NavLink>
                            <NavLink exact to="/" activeClassName="active" className="TopBarRight">หน้าแรก</NavLink>

                        </div>
                    </div >
                break;

            case "3":
                render_user = <div > SE ย่อย </div>
                break;

            case "4": //SE กลาง
                render_user =
                    <div className="App Sticky">
                        <div className="TopBar">
                            <NavLink exact to="/" className="LogoBar">SE Home Shop</NavLink>

                            <NavLink to="/signin" activeClassName="active" className="TopBarRight" onClick={this.logOut.bind(this)} >ออกจากระบบ</NavLink>
                            <NavLink to="/user" activeClassName="active" className="TopBarRight">
                                {this.state.get_user ? this.state.get_user.username : null}
                            </NavLink>
                            {this.state.get_user ? <img alt="PhotoUser" className="PhotoProfileBar" src={ip + this.state.get_user.user_image} />
                                : <img alt="PhotoUser" className="PhotoProfileBar" src={this.state.photo_profile} />}


                            <NavLink to="/frequency" activeClassName="active" className="TopBarRight">วางแผนการเพาะปลูก</NavLink>
                            <NavLink to="/sales_sum" activeClassName="active" className="TopBarRight">สรุปการขาย</NavLink>
                            <NavLink to="/product_information" activeClassName="active" className="TopBarRight">สินค้า</NavLink>
                            <NavLink exact to="/" activeClassName="active" className="TopBarRight">หน้าแรก</NavLink>

                        </div>
                    </div >
                break;

            case "5": //admin
                render_user =
                    <div className="App Sticky">
                        <div className="TopBar">
                            <NavLink exact to="/" className="LogoBar">SE Home Shop</NavLink>

                            <NavLink to="/signin" activeClassName="active" className="TopBarRight" onClick={this.logOut.bind(this)} >ออกจากระบบ</NavLink>
                            <NavLink to="/user" activeClassName="active" className="TopBarRight">
                                {this.state.get_user ? this.state.get_user.username : null}
                            </NavLink>
                            {this.state.get_user ? <img alt="PhotoUser" className="PhotoProfileBar" src={ip + this.state.get_user.user_image} />
                                : <img alt="PhotoUser" className="PhotoProfileBar" src={this.state.photo_profile} />}

                            <NavLink to="/frequency" activeClassName="active" className="TopBarRight">วางแผนการเพาะปลูก</NavLink>
                            <NavLink to="/sales_sum" activeClassName="active" className="TopBarRight">สรุปการขาย</NavLink>
                            <NavLink to="/manage_order" activeClassName="active" className="TopBarRight">ประวัติการซื้อ</NavLink>
                            <NavLink to="/show_cart" activeClassName="active" className="TopBarRight">ตะกร้าสินค้า</NavLink>
                            <NavLink to="/product_information" activeClassName="active" className="TopBarRight">สินค้า</NavLink>
                            <NavLink exact to="/" activeClassName="active" className="TopBarRight">หน้าแรก</NavLink>

                        </div>
                    </div >
                break;

            default:
                render_user = <div className="FontDanger"> เกิดข้อผิดพลาด 
                 <NavLink to="/signin" activeClassName="active" className="TopBarRight" onClick={this.logOut.bind(this)} >ออกจากระบบ</NavLink></div>
                break;
        }
        return render_user
    }

    logOut = (e) => {
        e.preventDefault()
        localStorage.removeItem('user_token')
        this.props.history.push('/')
    }

    render() {
        const loginRegLink = (
            <div className="App Sticky">
                <div className="TopBar">
                    <NavLink exact to="/" className="LogoBar">SE Home Shop</NavLink>

                    <NavLink to="/signin" activeClassName="active" className="TopBarRight">เข้าสู่ระบบ</NavLink>
                    <NavLink to="/signup" activeClassName="active" className="TopBarRight">ลงทะเบียน</NavLink>
                    <NavLink exact to="/" activeClassName="active" className="TopBarRight">หน้าแรก</NavLink>
                </div>
            </div >
        )

        const userLink = (
            <div className="App Sticky">
                <div className="TopBar">
                    {this.render_type(this.state.get_user ? this.state.get_user.user_type : null)}
                </div>
            </div >
        )

        return (

            <div>
                {localStorage.user_token ? userLink : loginRegLink}
            </div >
        )
    }
}
export default withRouter(Nav_bar);