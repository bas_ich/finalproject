import React, { Component } from 'react'
import '../App.css'
import {Link} from 'react-router-dom'

class HTimeline extends React.Component {
    render() {

        return (
            <div className="App">
                <div className="Row">
                        <ul className="progressbar">
                        <Link to="#0"><li activeClassName="active" className="Standat">ส่งใบสั่งซ์้อ</li></Link>
                        <Link to="#1"><li activeClassName="active" className="Standat">ยืนยันคำสั่งซื้อ</li></Link>
                        <Link to="#2"><li activeClassName="active" className="Standat">ใบเเจ้งหนี้</li></Link>
                        <Link to="#3"><li activeClassName="active" className="Standat">ใบเสร็จ</li></Link>
                        </ul>
                </div>
            </div>
        )
    }
}
export default HTimeline;