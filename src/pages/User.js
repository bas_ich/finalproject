import React, { Component } from 'react';
import { get, ip } from '../service/service';
import { user_token } from '../support/Constance';
import { Link } from 'react-router-dom'

class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            name: '',
            lastname: '',
            email: '',
            phone: '',
            address: '',
            user_type: 1,
            get_user: null,
            isInEdit: false,
            user_image: null,
            default_user_image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2S47oPrtWI_lK68iwye6EW3Q9GMRPoCQPw4vlObBssUl355pLMg"
        };
    }

    render_type = (user_type) => {
        let render_user
        switch (user_type) {
            case "1":
                render_user = <div > นักวิจัย </div>
                break;
            case "2":
                render_user = <div > ผู้ประกอบการ </div>
                break;
            case "3":
                render_user = <div > SE ย่อย </div>
                break;
            case "4":
                render_user = <div > SE กลาง  </div>
                break;
            case "5":
                render_user = <div > Admin </div>
                break;
            default:
                render_user = <div className="FontDanger"> เกิดข้อผิดพลาด </div>
                break;
        }
        return render_user
    }

    get_user = async () => {
        try {
            await get('show/show_user', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        get_user: result.result
                    })
                    setTimeout(() => {
                        console.log("get_user", result.result)
                    }, 500)
                } else {
                    window.location.href = "/";
                    //alert("user1"+result.error_message);
                }
            });
        } catch (error) {
            alert("get_user2" + error);
        }
    }

    componentWillMount() {
        this.get_user()
    }

    render() {
        return (
            <div className="App">
                <div className="FontHeader">ข้อมูลบัญชีผู้ใช้</div>
                <div className="Row">
                    <div className="Column Side">
                        {this.state.get_user ?
                            <img className="PhotoProfile" src={ip + this.state.get_user.user_image} alt="PhotoUser"/>
                            : <img className="PhotoProfile" src={this.state.default_user_image} alt="DefulatPhotoUser"/>}
                        <div className="FontBody">{this.state.get_user ? this.state.get_user.username : null}</div>
                        <div className="FontSide">{this.state.get_user ? this.render_type(this.state.get_user.user_type) : null}</div>
                        <ul>
                            <li><button className="BTNProcess"><Link to={"/user/edit_profile"}>แก้ไขประวัติ</Link></button></li>
                            <li><button className="BTNSuccess"><Link to={"/user/edit_password"}>แก้ไขรหัสผ่าน</Link></button></li>
                        </ul>
                    </div>
                    <div className="Column Middle">

                        <table>
                            <tbody >
                                <tr >
                                    <th > ประเภทผู้ใช้ </th>
                                    <td >
                                        <div>
                                            {this.state.get_user ? this.render_type(this.state.get_user.user_type) : null}
                                        </div>
                                    </td>
                                </tr >
                                <tr >
                                    <th > ชื่อผู้ใช้ </th>
                                    <td >
                                        <div>
                                            {this.state.get_user ? this.state.get_user.username : null}
                                        </div>
                                    </td>
                                </tr >
                                <tr >
                                    <th > ชื่อ </th>
                                    <td > {this.state.get_user ? this.state.get_user.name : null} </td>
                                </tr >

                                <tr >
                                    <th > นามสกุล </th>
                                    <td > {this.state.get_user ? this.state.get_user.lastname : null} </td>
                                </tr >

                                <tr >
                                    <th> E - mail </th>
                                    <td> {this.state.get_user ? this.state.get_user.email : null} </td>
                                </tr >

                                <tr >
                                    <th > เบอร์โทรศัพท์ </th>
                                    <td > {this.state.get_user ? this.state.get_user.phone : null} </td>
                                </tr >

                                <tr >
                                    <th > ที่อยู่ </th>
                                    <td > {this.state.get_user ? this.state.get_user.address : null} </td>
                                </tr >
                            </tbody>
                        </table >
                    </div>
                </div>
            </div>
        )
    }
}

export default User;