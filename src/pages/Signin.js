import React, { Component } from 'react';
import '../App.css';
import { get, post } from '../service/service';

class Signin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("Signin" + this.state);
    }

    get_users = async () => {
        let object = {
            username: this.state.username,
            password: this.state.password
        };
        try {
            await post(object, "user/user_login", null).then(res => {
                if (res.success) {
                    // alert(res.token)
                    localStorage.setItem("user_token", res.token);
                    window.location.href = "/";
                    console.log("Signin" + res.token);
                } else {
                    alert(res.error_message);
                }
            });
        } catch (error) {
            alert(error);
        }
        console.log("Signin" + this.state);
    }

    facebook = async () => {
        console.log("facebook");
        try {
            await get("auth/facebook", null).then(res => {
                if (res.success) {
                    //localStorage.setItem("user_token",res.token);
                    //window.location.href = "/";
                    console.log("facebook : " + res.token);
                }
                else {
                    window.location.href = "/signup";
                }
            });
        } catch (error) {
            alert("alert_facebook" + error);
        }
    }

    ChangePage = () => {
        window.location.href = "/signup"
    }

    render() {
        return (
            <div className="App" >
                <div className="FontHeader"> เข้าสู่ระบบ </div>
                <div className="Row">
                    <div className="Column Side"></div>
                    <div className="Column Side"></div>
                    <div className="Column Middle">
                        <div className="FontSignin"> ชื่อผู้ใช้งาน </div>
                        <input type="text" id="username" placeholder="กรอกชื่อบัญชีผู้ใช้" name="username" onChange={this.handleChange} />
                        <div className="FontSignin"> รหัสผ่าน </div>
                        <input type="password" id="password" placeholder="กรอกรหัสผ่าน" name="password" onChange={this.handleChange} />
                        <button className="BTNSignin" onClick={() => this.get_users(this.state.username, this.state.password)}>
                            เข้าสู่ระบบ
                        </button>
                        <button className="BTNSignin" onClick={() => this.ChangePage()}>สร้างบัญชีผู้ใช้</button>
                    </div>
                    <div className="Column Side"></div>
                    <div className="Column Side"></div>
                </div>
            </div >
        );
    }
}

export default Signin;