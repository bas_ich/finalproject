import React, { Component } from "react";
import ReactHighcharts from "react-highcharts";
import { element } from "prop-types";
import { Col ,Row} from "reactstrap";
class Frequency_MaterialChart extends Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }

  mapdata = () => {
    let data = [];
    this.props.rang.map((element, index) => {
    
      data.push({
        name: "ช่วงที่" + element.name,
        data: element.data
      });
    });
    this.setState({ data: data });
  };

  componentWillMount() {
    this.mapdata();
  }

  render() {
    const options = {
      chart: {
        type: "column",
        style: {
          fontFamily: "Kunlasatri"
        }
      },
      title: {
        text: this.props.nameSE,
        style: {
          fontSize: 30
        }
      },
      legend: {
        layout: "vertical",
        align: "right",
        verticalAlign: "top",
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: "#FFFFFF",
        shadow: true
      },
      xAxis: {
        categories: [
         'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ค.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
        ],
        crosshair: true
        ,
        labels : {
          style: {
            "fontSize": "18px"
          }
        }  },
        credits: {
          enabled: false
        },
      yAxis: {
        min: 0,
        title: {
          text: "ปริมาณ (กิโลกรัม/ครั้ง)",
          style: {
            "fontSize": "18px"
          }
        }
        ,
        labels : {
          style: {
            "fontSize": "18px"
          }
        }  },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0"> ส่งมอบครั้งที่ {series.name}: </td>' +
          '<td style="padding:0"><b> ปริมาณ {point.y} (กิโลกรัม/ครั้ง)</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: this.props.rang

    };

    return (
      

       <div style={{ marginTop: 20, textAlign: "center" }}>
       <row>
       <col>
          <ReactHighcharts config={options} />
        </col>
       </row>
   </div>

   
    );
  }
}

export default Frequency_MaterialChart;
