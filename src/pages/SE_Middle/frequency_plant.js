import React, { Component } from 'react';
import { post } from '../../service/service'
import { user_token } from '../../support/Constance'

class FrequencyPlant extends Component {
    constructor(props) {
        super(props)
        this.state = {
            plant_name: [],
            plant_chart: [],

        }
    }
    componentWillMount() {
        this.get_order()
    }

    get_order = async () => {
        let params = { plant_name: this.props.data_plant }
        try {
            await post(params, 'neutarlly/get_chart_frequency_all', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        plant_chart: result.result,
                    })
                    setTimeout(() => {
                        console.log("get_chart_frequency_all", result.result)
                    }, 500)
                } else {
                    // window.location.href = "/sales_sum";
                    // alert(result.error_message)
                    console.log("get_chart_frequency_all", result.result)
                }
            });
        } catch (error) {
            alert("get_chart_frequency_all" + error);
        }
    }

    render() {
        return (
            <div className="App">
                
            </div>
        )
    }
}
export default FrequencyPlant;