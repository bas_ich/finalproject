import React, { Component } from 'react'
import { get, post, ip } from '../../service/service';
import { user_token } from '../../support/Constance';
import moment from 'moment'
import { NavLink } from 'react-router-dom';

class SalesSum extends Component {
    constructor(props) {
        super(props);
        this.state = {
            get_order: [],
        }
    }
    componentWillMount() {
        this.get_order();
    }

    get_order = async () => {
        try {
            await get('neutarlly/get_order_all', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        get_order: result.result
                    })
                    setTimeout(() => {
                        console.log("get_order", result.result)
                    }, 500)
                } else {
                    window.location.href = "/";
                    //alert("user1"+result.error_message);
                }
            });
        } catch (error) {
            alert("get_order" + error);
        }
    }

    render_status = (order_status) => {
        let render_tag

        switch (order_status) {
            case 0:
                render_tag = <div>
                    <div className="FontWarning" > กำลังดำเนินการ </div>
                </div>
                break;
            case 1:
                render_tag = <div>
                    <div className="FontSuccess"> สำเร็จแล้ว </div>
                </div>
                break;
            default:
                render_tag = <div>
                    <div className="FontDanger"> เกิดข้อผิดพลาด </div>
                </div>
                break;
        }
        return render_tag
    }

    render() {
        return (
            <div className="App">
                <div className="FontHeader">สรุปการขาย</div>
                <div className="Row">
                <div className="Column Side"></div>
                    <div className="Column Middle">
                        <table>
                            <tr>
                                <th>NO.</th>
                                <th>Date</th>
                                <th>OrderID</th>
                                <th>Status</th>
                                <th>Name</th>
                                <th>Detail</th>
                            </tr>
                            {this.state.get_order.map((element, index) => {
                                return (
                                    <tr>
                                        <td>{index + 1}</td>
                                        <td>{moment(element.order_date).utc().format("DD/MM/YYYY, HH:mm")}</td>
                                        <td>{element.order_id}</td>
                                        <td>{this.render_status(element.order_status)}</td>
                                        <td>{element.trader_id}</td>
                                        <td><NavLink to={"/sales_sum/order?order_id=" + element.order_id} className="Link">รายละเอียด</NavLink></td>
                                    </tr>
                                )
                            })}
                        </table>
                    </div>
                    <div className="Column Side"></div>
                </div>
            </div>
        )
    }
}
export default SalesSum;