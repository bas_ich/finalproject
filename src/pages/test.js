import React, { Component } from "react";
import { OffCanvas, OffCanvasMenu, OffCanvasBody } from "react-offcanvas";

export default class Test extends Component {
  componentWillMount() {
    // sets the initial state
    this.setState({
      isMenuOpened: false
    });
  }


  handleClick() {
    // toggles the menu opened state
    this.setState({ isMenuOpened: !this.state.isMenuOpened });
  }

  render() {
    return (
      <div className="App">
        <OffCanvas
          style={{
            height: "100%",
            position: "fixed",
            top: "0",
            left: "0",
            background: "#111",
            overflowX: "hidden",
            transition: "0.5s",
            paddingTop: "60px"
          }}
          isMenuOpened={this.state.isMenuOpened}
          width={500}
        // width={800}
        // height={100}
        // transitionDuration={300}
        // effect={"parallax"}
        // isMenuOpened={this.state.isMenuOpened}
        // position={"right"}
        >
          <OffCanvasBody>
            <button onClick={this.handleClick.bind(this)}>
              ทำการสั่งซื้อจาก SE ย่อย
            </button>
            <input type="money"/>บาท
          </OffCanvasBody>


          <OffCanvasMenu
            style={{ marginTop: "50px" }}
          >
            <button style={{float:"right"}} onClick={this.handleClick.bind(this)}>
                  X
              </button>
            <h4>ชื่อวัตถุดิบ</h4>
            <h5>จำนวนวัตถุดิบทั้งหมด</h5>
            <h5 style={{color:"red"}}>จำนวนที่ต้องซื้อ</h5>
                
              <table>
                <tr>
                  <th>ชื่อ SE</th>
                  <th>จำนวนวัตถุดิบที่มีในสต๊อก</th>
                  <th>ค่าขนส่ง</th>
                  <th>ช่วงส่งมอบ</th>
                </tr>
                <tr>
                  <td>SE-1</td>
                  <td>10000</td>
                  <td>20</td>
                  <td>มกราคม</td>
                </tr>

                <tr>
                  <td>SE-2</td>
                  <td>545400</td>
                  <td>200</td>
                  <td>มกราคม</td>
                </tr>

                <tr>
                  <td>SE-3</td>
                  <td>100500</td>
                  <td>888</td>
                  <td>ธันวาคม</td>
                </tr>

                <tr>
                  <td>SE-4</td>
                  <td>10000</td>
                  <td>20</td>
                  <td>มกราคม</td>
                </tr>

                <tr>
                  <td>SE-5</td>
                  <td>10000</td>
                  <td>20</td>
                  <td>มกราคม</td>
                </tr>
              </table>
          </OffCanvasMenu>
        </OffCanvas>
      </div>

    );
  }

}