import React, { Component } from 'react';

class Home extends Component {

    render() {
        return (
            <div className="App">
                <div className="Row">
                    <div className="col-12">
                        <div className="HeaderArea">
                            <div className="Row">
                                <div className="col-6" style={{ backgroundColor: "black" }}>
                                    <img alt="Product"/>
                                </div>
                                <div className="col-1"></div>
                                <div className="col-5">
                                    <h2>ชื่อสินค้า</h2>
                                    <h4>ราคาขายปลีก</h4>
                                    <button className="BTN_Buy">ซื้อสินค้า</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="Row">
                    <div className="col-1" >  </div>
                    <div className="col-10">
                        <div className="HeaderAreaCard">
                            <img alt="Product"/>
                            <h4>สินค้า 1</h4>
                            <h5>ราคาขายปลีก</h5>
                            <button>
                                รายละเอียด
                            </button>
                        </div>

                    </div>
                    <div className="col-1"> </div>
                </div>
            </div>

        )
    }
}

export default Home;