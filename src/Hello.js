import React from 'react';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;


pdfMake.fonts = {
    THSarabunNew: {
        normal: 'THSarabunNew.ttf',
        bold: 'THSarabunNew Bold.ttf',
        italics: 'THSarabunNew Italic.ttf',
        bolditalics: 'THSarabunNew BoldItalic.ttf'
    },
    Roboto: {
        normal: 'Roboto-Regular.ttf',
        bold: 'Roboto-Medium.ttf',
        italics: 'Roboto-Italic.ttf',
        bolditalics: 'Roboto-MediumItalic.ttf'
    }
}

class Hello extends React.Component {

    pdf() {

        var docDefinition = {
            content: [
                { text: 'ใบแจ้งหนี้', fontSize: 24, bold: true, alignment: 'center' },
                {
                    text: 'บริษัทอะไรสักอย่าง....',
                    style: 'header'
                },
                'ที่อยู่...เลขประจำตัวผู้เสียภาษี....\n\n',
                {
                    style: 'tableExample',
                    table: {
                        widths: [30, 320, 75, 75],
                        body: [
                            [{ text: 'ชื่อผู้สั่ง....\n\nที่อยู่....\n', colSpan: 2 }, {}, ['เลขที่...', 'SE-123'], ['วันที่...', '20/06/2060']],
                            [{ text: 'ลำดับที่', alignment: 'center' }, { text: 'รายการ', alignment: 'center' }, 'จำนวน', 'ราคา'],
                            [[{ text: '1', alignment: 'center' }, { text: '2', alignment: 'center' }], '', '', '']
                        ]
                    },
                },
            ],
            defaultStyle: {
                font: 'THSarabunNew'
            },
            styles: {
                header: {
                    fontSize: 18,
                    bold: true
                },
                subheader: {
                    fontSize: 15,
                    bold: true
                },
                tableExample: {
                    margin: [0, 5, 0, 15]
                },
            }
        };
        pdfMake.createPdf(docDefinition).open()

    }

    render() {
        return (
            <div>
                <br />
                <input placeholder="Input" />{' '}
                <button color="secondary">ADD</button><br />
                <button color="secondary" onClick={() => { this.pdf() }}>viwePDF</button>
            </div>

        )
    }
}
export default Hello;